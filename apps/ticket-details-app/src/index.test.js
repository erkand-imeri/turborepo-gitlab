test('adds 1 + 2 to equal 3', () => {
    const customVar = 1 + 2;
  expect(customVar).toBe(3);
});

test('adds 3 + 2 to equal 5', () => {
    const customVar = 3 + 2;
  expect(customVar).toBe(5);
});