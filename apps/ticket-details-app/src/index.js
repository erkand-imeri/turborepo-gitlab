import express from 'express';
const app = express();
import { TICKET_DETAILS_PORT } from 'app-config';


app.get('/', async (req, res) => {
    // Make a request for a user with a given ID
// const response = await axios.get('https://api.publicapis.org/entries').catch(error => console.log(error));

res.send(`Ticket Details App response:`);
})

app.listen(TICKET_DETAILS_PORT, () => {
  console.log(`Example app listening on port ${TICKET_DETAILS_PORT}`)
})