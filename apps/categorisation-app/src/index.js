import express from 'express';
const app = express();
import _ from 'lodash';
import axios from 'axios';

import { CATEGORISATION_APP } from 'app-config';

app.get('/', async (req, res) => {
   const response = await axios.get('https://api.publicapis.org/entries').catch(error => console.log(error));
   var other = _.concat([2,4,5], 2, [3], [[4]]);

res.send(`Categorisation App response: ${response} and other: ${other}`);
})

app.listen(CATEGORISATION_APP, () => {
  console.log(`Example app listening on port ${CATEGORISATION_APP}`)
})