test('adds 1 + 2 to equal 3', () => {
    const customVar = 1 + 2;
  expect(customVar).toBe(3);
});
test('adds 2 + 2 to equal 4', () => {
    const customVar = 2 + 2;
  expect(customVar).toBe(4);
});